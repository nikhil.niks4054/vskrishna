
(function ($) {
  
    "use strict";
      //Common Json Fetch Function
      function fetchJson(){
        fetch("./data.json")
          .then(response => response.json())
          .then(data => {
            //navigation(data);
            //createSlider(data);
            //displayAboutSection(data);
            //displayImageContentSection(data);
            //cardsBlockSection(data);
            //testimonialsSection(data);
            //contactSection(data);
            //footer(data);
          });
      }
  
      //Navigation Data
      function navigation(data) {
        let navigationSelector = $('.navbar-nav-json');
        for (let i = 0; i < data.navigation.length; i++) {
          let dataNavigation = `
            <li class="nav-item">
              <a class="nav-link click-scroll" href="${data.navigation[i].link}">${data.navigation[i].title}</a>
            </li>
          `;
          navigationSelector.append(dataNavigation);
        }
      }

      //Slider Data
      function createSlider(data) {
        let sliderSelector = $('.banner-carousel-json');
        for (let i = 0; i < data.banner.length; i++) {
          var checkActive = i === 0 ? "active" : "";
          let dataSlider = `
          <div class="carousel-item ${checkActive}" style="background-image: url('${data.banner[i].image}')">
            <div class="carousel-caption d-flex flex-column justify-content-end">
                <h1>${data.banner[i].title}</h1>
                <p>${data.banner[i].description}</p>
            </div>
          </div>
          `;
          sliderSelector.append(dataSlider);
        }
      }

      //About Us Data
      function displayAboutSection(data){
        let aboutSection = $(".about-us-section-json");
        let dataAbout = `
          <div class="col-lg-10 col-12 text-center mx-auto">
              <h2 class="mb-3">${data.about.title}</h2>
          </div>
          <div class="col-lg-10 col-12 text-center mx-auto">
              <p>${data.about.description}</p>
          </div>
        `;
        aboutSection.append(dataAbout);
      }
      
      //Image Content Data
      function displayImageContentSection(data){
        let imageContentSection = $(".image-content-section-json");
        let dataImageContent = `
          <div class="col-lg-6 col-12 mb-5 mb-lg-0">
              <img src="${data.imageContent.image}" class="custom-text-box-image img-fluid" alt="">
          </div>
          <div class="col-lg-6 col-12">
              <div class="custom-text-box">
                  <h2 class="mb-2">${data.imageContent.title}</h2>
                  <h5 class="mb-3">${data.imageContent.shortDescription}</h5>
                  <p class="mb-4"><button class="btn btn-outline-primary btn-lg"> <span class="badge badge-secondary">281</span> Plots</button> <button class="btn btn-outline-primary btn-lg">38 Acres</button> </p>
                  <p class="mb-0"><button class="btn btn-outline-primary btn-lg"> <span class="badge badge-secondary">10</span> Amenities</button></p>
              </div>
          </div>
        `;
        imageContentSection.append(dataImageContent);
      }

      //Cards Block Data
      function cardsBlockSection(data) {
        let cardsBlockSection = $('.cards-block-json');
        let titleCardBlock = `
            <div class="col-lg-12 col-12 text-center mb-4">
                <h2>${data.cardsBlock.mainTitle}</h2>
            </div>
        `;
        cardsBlockSection.append(titleCardBlock);
        for (let i = 0; i < data.cardsBlock.cards.length; i++) {
          let dataCards = `
          <div class="col-lg-4 col-md-6 col-12 mb-4 mb-lg-0">
                <div class="custom-block-wrap">
                    <img src="${data.cardsBlock.cards[i].image}" class="custom-block-image img-fluid" alt="">
                    <div class="custom-block">
                        <div class="custom-block-body">
                            <h5 class="mb-3">${data.cardsBlock.cards[i].title}</h5>
                            <p>${data.cardsBlock.cards[i].description}</p>
                        </div>
                    </div>
                </div>
            </div>
          `;
          cardsBlockSection.append(dataCards);
        }
      }

      //Testimonials Block Data
      function testimonialsSection(data) {
        let testimonialsSection = $('.testimonials-json');
        let titletestimonialsBlock = `
            <div class="col-lg-8 col-12 mx-auto">
                <h2 class="mb-lg-3 text-center">${data.testimonialsBlock.mainTitle}</h2>
                <div id="testimonial" class="testimonials-wrapper"><ul></ul></div>
            </div>
        `;
        testimonialsSection.append(titletestimonialsBlock);
        for (let i = 0; i < data.testimonialsBlock.testimonials.length; i++) {
          let dataTestimonials = `
            <li><span class="image"><img src="${data.testimonialsBlock.testimonials[i].image}"></span><span class="text">${data.testimonialsBlock.testimonials[i].description}</span></li>
          `;
          testimonialsSection.find(".testimonials-wrapper > ul").append(dataTestimonials);
        }
      }

      //Contact Us Data
      function contactSection(data){
        let contactSection = $(".contact-json");
        let dataContact = `
        <div class="offset-lg-1 col-lg-4 col-12 mb-5 mb-lg-0">
            <div class="contact-info-wrap">
                <h2>${data.getInTouch.title}</h2>
                <div class="contact-info">
                    <h5 class="mb-3">${data.getInTouch.shortTitle}</h5>
                    <p class="d-flex mb-2"><i class="bi-geo-alt me-2"></i>${data.getInTouch.address}</p>
                    <p class="d-flex mb-2"><i class="bi-telephone me-2"></i><a href="tel: ${data.getInTouch.phone}">${data.getInTouch.phone}</a>
                    </p>
                    <p class="d-flex"><i class="bi-envelope me-2"></i><a href="mailto:${data.getInTouch.email}">${data.getInTouch.email}</a>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-lg-7 col-12">
            <div class="mapouter"><div class="gmap_canvas"><iframe class="gmap_iframe" width="100%" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="${data.getInTouch.iframeLink}"></iframe></div></div>
        </div>
        `;
        contactSection.append(dataContact);
      }

      //Contact Us Data
      function footer(data){
        let footer = $(".footer-json");
        let dataFooter = `
        <div class="col-lg-6 col-md-7 col-12">
            <p class="copyright-text mb-0">${data.footer.copyright}</p>
        </div>
        <div class="col-lg-6 col-md-5 col-12 d-flex justify-content-center align-items-center mx-auto">
            <ul class="social-icon">
                <li class="social-icon-item">
                    <a href="${data.footer.facebookLink}" class="social-icon-link bi-facebook"></a>
                </li>
                <li class="social-icon-item">
                    <a href="${data.footer.instaLink}" class="social-icon-link bi-instagram"></a>
                </li>
               
                <li class="social-icon-item">
                    <a href="${data.footer.youtubeLink}" class="social-icon-link bi-youtube"></a>
                </li>
            </ul>
        </div>
        `;
        footer.append(dataFooter);
      }
      
  
      fetchJson();
  })(window.jQuery);

   // <li class="social-icon-item">
                //     <a href="${data.footer.linkedinLink}" class="social-icon-link bi-linkedin"></a>
                // </li>